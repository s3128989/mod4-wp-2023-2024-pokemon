package nl.utwente.mod4.pokemon.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class ResourceCollection<T extends NamedEntity> {

    public HashMap<String,Object> meta;
//    public HashMap<String,Object> links;
    public T[] data;

    public ResourceCollection() {
        data = null;
        meta = new HashMap<>();
//        links = new HashMap<>();
    }

    public ResourceCollection(T[] resources, int pageSize, int pageNumber, int total) {
        meta = new HashMap<>();
//        links = new HashMap<>();

        meta.put("total",  total);
        meta.put("pageNumber", pageNumber);
        meta.put("pageSize", pageSize);

        data = resources;
    }

}
